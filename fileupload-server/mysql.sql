CREATE TABLE `upload_files` (
  `id` varchar(40) NOT NULL DEFAULT '' COMMENT 'id',
  `name` varchar(100) DEFAULT NULL COMMENT '文件名',
  `path_name` varchar(60) DEFAULT NULL COMMENT '物理名',
  `type` varchar(60) DEFAULT NULL COMMENT '类型',
  `size2` varchar(20) DEFAULT NULL COMMENT '大小字符串',
  `upload_date` date DEFAULT NULL COMMENT '上传时间',
  `size` int(10) DEFAULT NULL COMMENT '大小',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;