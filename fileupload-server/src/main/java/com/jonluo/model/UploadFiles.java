package com.jonluo.model;

import java.io.Serializable;
import java.util.Date;

public class UploadFiles  implements Serializable {
    /**
    * id
    */
    private String id;

    /**
    * 文件名
    */
    private String name;

    /**
    * 物理名
    */
    private String pathName;

    /**
    * 类型
    */
    private String type;

    /**
    * 大小字符串
    */
    private String size2;

    /**
    * 上传时间
    */
    private Date uploadDate;

    /**
    * 大小
    */
    private Integer size;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize2() {
        return size2;
    }

    public void setSize2(String size2) {
        this.size2 = size2;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}