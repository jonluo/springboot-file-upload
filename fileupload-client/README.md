# 在springboot的项目加入配置
在application.properties加入配置
```
#文件上传服务器地址
files-server-url=http://localhost:9000
spring.http.multipart.max-file-size=100MB
spring.http.multipart.max-request-size=200MB
```

# 在static的文件夹加入doc资源文件夹

# 在代码中加入DocLoadController.java文件

# 在所用到的html中加入
```
<!--文件上传控件的位置-->
<div id="file-div"></div>
<script>
    $(function () {
        var fileData ={
            "type":"file",       //文件 "photo" 图片 默认为photo
            "more": 4,           //数字，最多上传多少个文件 ，默认为1
            "uploadAction":true, //true 能上传 false不能上传,默认为false
            "filenames":"",       //文件名字符串，多个以；隔开
             "index":""  //一个页面多个组件要设置
        }

        $("#file-div").load("${pageCtx('ctxPath')}/doc/index",fileData);
    });
</script>
```

```注意``` 上传后的文件id保存在一个隐藏域里 可以通过其id为“filelist”+fileData.index即```$("#filelist+fileData.index").val()```获取
