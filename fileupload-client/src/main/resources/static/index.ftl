<!DOCTYPE html>
<html lang="en">
<head>
    <title>Protected Resource Service</title>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
</head>
<body>
    <h2>文件上传</h2>
    <div id="file-div"></div>
</body>
</html>
<script>
    $(function () {
        var fileData ={
            "type":"file",       //文件 "photo" 图片 默认为photo
            "more": 1,           //数字，最多上传多少个文件 ，默认为1
            "uploadAction":true, //true 能上传 false不能上传,默认为false
            "filenames":"",      //文件名字符串，多个以；隔开
            "index":"1"  //一个页面多个组件要设置
        }

        $("#file-div").load("/doc/index",fileData);
    });
</script>